<?php
 
/*
 * Following code will update a product information
 * A product is identified by product id (pid)
 */
 
// array for JSON response
$response = array();
 
// check for required fields
if (!empty($_POST['pid']) and !empty($_POST['name']) and !empty($_POST['price']) and !empty($_POST['description'])) {
 
    $pid = $_POST['pid'];
    $name = $_POST['name'];
    $price = $_POST['price'];
    $description = $_POST['description'];
 
	$host="localhost";      //replace with your hostname 
	$username="root";       //replace with your username 
	$password="";           //replace with your password 
	$db_name="android";     //replace with your database 

	//open connection to mysql db
	$connection = new mysqli($host,$username,$password,$db_name);
	
	// check connection 
	if (mysqli_connect_errno()) {
		$response["success"] = 0;
		$response["message"] = "Error Connection failed " .mysqli_connect_error();
		// echoing JSON response
		echo json_encode($response);
		exit();		
	}

	if(!$connection->set_charset("utf8")) {  
		$response["success"] = 0;
		$response["message"] = "Error loading character set utf8:". $connection->error;
 
		// echoing JSON response
		echo json_encode($response);
		//close the db connection
		$connection->close();
		exit(); 
	}
 
    // mysql update row with matched pid
	$sql = "UPDATE products SET name = '$name', price = '$price', description = '$description' WHERE pid = $pid";
	$result = $connection->query($sql);
	 
    // check if row updated or not
    if ($connection->affected_rows > 0) {
        // successfully updated
        $response["success"] = 1;
        $response["message"] = "Product successfully updated.";
 
        // echoing JSON response
        echo json_encode($response);
    } else {
 
    }
} else {
    // required field is missing
    $response["success"] = 0;
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>