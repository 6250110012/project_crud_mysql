import 'package:flutter/material.dart';
import 'package:path/path.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column();
  }
}

class About extends StatelessWidget {
  const About({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('About'),
        backgroundColor: Colors.purple,

      ),
      body: Column(
        children: [
          image(
            assetImage: AssetImage("assets/images/wannaporn.jpg"),
          ),
          SizedBox(height: 20),
          name(
            text: Text(
              "Wannaporn Thongkaw (Pakbung) 6250110012  ICM",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(height: 50),
          image(
            assetImage: AssetImage("assets/images/woralak.jpg"),
          ),
          SizedBox(height: 20),
          name(
            text: Text(
              "Woralak Samart (Nasria) 6250110028  ICM",
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }
}

class name extends StatelessWidget {
  const name({Key? key, required this.text}) : super(key: key);

  final Text text;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: FlatButton(
        padding: EdgeInsets.all(20),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
        color: Colors.black12,
        onPressed: () {},
        child: Row(
          children: [
            Image.asset(
              "assets/images/user.png",
              width: 35,
            ),
            SizedBox(width: 30),
            Expanded(
              child: text,
            ),
          ],
        ),
      ),
    );
  }
}

class image extends StatelessWidget {
  const image({Key? key, required this.assetImage}) : super(key: key);

  final AssetImage assetImage;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: SizedBox(
        height: 150,
        width: 150,
        child: CircleAvatar(
          backgroundImage: assetImage,
        ),
      ),
    );
  }
}
